/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

console.log('variables defined');
// Canvas variables
var canvas;
var ctx;
var backgroundColour;

// Tile variables
var tilesReady;
var grass;
var sand;
var water;

// Map variables
var mapWidth;
var mapHeight;
var mapArray;
var heightArray;

var defaultCameraX = 0;
var defaultCameraY = 0;
var cameraModifierX = 0;
var cameraModifierY = 0;

// Control variables
var keysDown;
var keyboardScrollSpeed;
var keyboardScrollBoost;
var mousePos = 0;
var mouseOverX = 0;
var mouseOverY = 0;

var paintType; // the type of tile the user has selected to paint

// Time
var then;
var now;
var delta;

// Tile class
function Tile(type, height) {
    // Add object properties like this
    this.type = type;
    this.height = height;
}

var initCanvas = function() {
    console.log('initCanvas called');
    // Create the canvas
    canvas = document.createElement("canvas");
    ctx = canvas.getContext("2d");
    canvas.width = 1280;
    canvas.height = 720;
    document.body.appendChild(canvas);

    backgroundColour = "rgb(0,0,65)";
};

var initTiles = function() {
    console.log('initTiles called');
    tilesReady = false;
    tilesLoaded = 0;
    tilesTotal = 0;

    var assetLoad = function() {
        ctx.fillStyle = backgroundColour;
        ctx.fillRect(0, 0, canvas.width, canvas.height);


        ctx.fillStyle = "rgb(250, 250, 250)";
        ctx.font = "20px Helvetica";
        ctx.textAlign = "left";
        ctx.textBaseline = "top";
        ctx.fillText("Loading: Images " + tilesLoaded + "/" + tilesTotal, 20, 20);
    };

    grass = new Image();
    tilesTotal++;
    grass.onload = function() {
        tilesLoaded++;
        assetLoad();
    };
    grass.src = "images/grass.png";

    water = new Image();
    tilesTotal++;
    water.onload = function() {
        tilesLoaded++;
        assetLoad();
    };
    water.src = "images/water.png";

    sand = new Image();
    tilesTotal++;
    sand.onload = function() {
        tilesLoaded++;
        assetLoad();
    };
    sand.src = "images/sand.png";

    assetLoad();

//    while (tilesLoaded !== tilesTotal) {
//        tilesReady = false;
//    }
//    tilesReady = true;

};

var initMap = function() {
    console.log('initMap called');
    // Set map dimentions
    mapWidth = 16;
    mapHeight = 16;

// Set tile resolution
    tileWidth = 64;
    tileHeight = 32;

    // Create Array to represent tile contents
    mapArray = new Array(mapWidth);
    for (var i = 0; i < mapWidth; i++) {
        mapArray[i] = new Array(mapHeight);
        for (var j = 0; j < mapHeight; j++) {
            mapArray[i][j] = new Tile("grass", 0);
        }
    }

    // Set location to default to map center
    defaultCameraX = (mapWidth / 2) * 64 - (canvas.width / 2);
    defaultCameraY = (canvas.height / 2);
};

var handleKeyboard = function() {
    // Handle keyboard controls
    keyboardScrollSpeed = 10;

    keysDown = {};

    addEventListener("keydown", function(e) {
        keysDown[e.keyCode] = true;
    }, false);

    addEventListener("keyup", function(e) {
        delete keysDown[e.keyCode];
    }, false);
};

var handleMouse = function() {
    // Handle mouse controls
    addEventListener("mousedown", doMouseDown, false);
    addEventListener("mouseup", doMouseUp, false);
    canvas.addEventListener("mousemove", function(evt) {
        mousePos = getMousePos(canvas, evt);
    }, false);

    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    }

    function doMouseDown(event) {
        // paint tiles onto map
        if ((mouseOverX >= 0) && (mouseOverY >= 0) && (mouseOverX <= mapWidth) && (mouseOverY <= mapHeight)) {
            mapArray[mouseOverX][mapWidth - 1 - mouseOverY].type = paintType;
        }
    }

    function doMouseUp(event) {
        backgroundColour = "rgb(0,0,65)";
    }

};

var update = function() {
    console.log('update called');

    var getKeyboard = function() {
        var keyboardScrollBoostSpeed = 10;
        keyboardScrollBoost = false;
        if (16 in keysDown) { // Player holding shift
            keyboardScrollBoost = true;
        } else {
            keyboardScrollBoost = false;
        }
        if (68 in keysDown) { // Player holding d
            if (keyboardScrollBoost === true) {
                cameraModifierX += (keyboardScrollSpeed + keyboardScrollBoostSpeed);
            } else {
                cameraModifierX += keyboardScrollSpeed;
            }
        }
        if (65 in keysDown) { // Player holding a
            if (keyboardScrollBoost === true) {
                cameraModifierX -= (keyboardScrollSpeed + keyboardScrollBoostSpeed);
            } else {
                cameraModifierX -= keyboardScrollSpeed;
            }
        }
        if (87 in keysDown) { // Player holding w
            if (keyboardScrollBoost === true) {
                cameraModifierY += (keyboardScrollSpeed + keyboardScrollBoostSpeed);
            } else {
                cameraModifierY += keyboardScrollSpeed;
            }
        }

        if (83 in keysDown) { // Player holding s
            if (keyboardScrollBoost === true) {
                cameraModifierY -= (keyboardScrollSpeed + keyboardScrollBoostSpeed);
            } else {
                cameraModifierY -= keyboardScrollSpeed;
            }
        }

        if (49 in keysDown) { // Player holding 0
            paintType = "water";
        }
        if (50 in keysDown) { // Player holding 0
            paintType = "sand";
        }
        if (51 in keysDown) { // Player holding 0
            paintType = "grass";
        }
    };
    var getMouse = function() {
        mouseOverY = ((mousePos.y * 2) - ((mapHeight * 64) / 2) + mousePos.x) / 2;
        mouseOverX = mousePos.X - mouseOverY;
        mouseOverY = mouseOverY / 64;
        mouseOverX = mouseOverX / 32;

        var x = mousePos.x - defaultCameraX + cameraModifierX;
        var y = mousePos.y - defaultCameraY - cameraModifierY;

        //mouseOverX = ((y + x / 2) / 32) - (20 - mapWidth);
        mouseOverX = ((y + x / 2) / 32) - (20 - mapWidth);
        mouseOverY = ((y - x / 2) / 32) + 20;

        // Convert the value to an integer without rounding
        mouseOverX = Math.floor(mouseOverX);
        mouseOverY = Math.floor(mouseOverY);
    }; // Converts the mouse screen position into a tile map tile coordinate

    getKeyboard();
    getMouse();
}; // Update game objects

var render = function() {
    console.log('render called');
    ctx.fillStyle = backgroundColour;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    var drawTiles = function() {
        var xPosition;
        var yPosition;
        var currentTile;
        var drawTile;

        var skipLeft;
        var skipRight;
        var skipAbove;
        var skipBelow;

        var xSkip = 0;
        var ySkip = 0;

        var tileNumber = 0;



        // Tile Loop
        for (var i = 0 + xSkip; i < mapWidth; i++) {
            for (var j = mapHeight - ySkip; j >= 1; j--) {
                // Initilise tile draw call
                drawTile = true;
                skipLeft = false;
                skipRight = false;
                skipAbove = false;
                skipBelow = false;
                // What tile are you drawing?
                if (mapArray[i][j - 1].type === "water") {
                    currentTile = water;
                }
                if (mapArray[i][j - 1].type === "grass") {
                    currentTile = grass;
                }
                if (mapArray[i][j - 1].type === "sand") {
                    currentTile = sand;
                }


                // Calculate where to draw the tile
                xPosition = ((j * tileWidth / 2) + (i * tileWidth / 2));
                yPosition = ((i * tileHeight / 2) - (j * tileHeight / 2));

                if (currentTile.height !== 32) {
                    yPosition = yPosition - (currentTile.height - 32);
                }

                // advanced clipping - this method skips sections of the loop which aren't on the screen
                // skip tiles left of screen
                if ((cameraModifierX + defaultCameraX) >= xPosition + 32) {
                    j = 0;
                    drawTile = false;
                }

                // skip tiles right of screen
                if ((cameraModifierX + defaultCameraX) <= xPosition - canvas.width - 32) {
                    ySkip++;
                    drawTile = false;
                }

                // skip tiles above the screen
                if (-(cameraModifierY + defaultCameraY) - 32 >= yPosition) {
                    xSkip = j + 1;
                    drawTile = false;
                }

                // skip tiles below the screen
                if (-(cameraModifierY + defaultCameraY) + canvas.height <= yPosition) {
                    j = 0;
                    drawTile = false;
                }

                // draw the tile
                if (drawTile === true) {
                    ctx.drawImage(currentTile, xPosition - (cameraModifierX + defaultCameraX) - (tileWidth / 2), yPosition + (cameraModifierY + defaultCameraY) + (mapArray[i][j - 1].height * 16));
                }
            }
            // Add skip values now we have reached the end of the row
            xSkip--;
        }
        ;
    };
    var drawDeveloperOverlay = function() {
        // set text type
        ctx.fillStyle = "rgb(250, 250, 250)";
        ctx.font = "20px Helvetica";
        ctx.textAlign = "left";
        ctx.textBaseline = "top";

        // display xy location
        ctx.fillText("cameraPos: " + cameraModifierX + "," + cameraModifierY, 10, 10);
        ctx.fillText("mousePos: " + Math.round(mousePos.x) + "," + Math.round(mousePos.y), 10, 25);

        // display which tile mouse is over
        ctx.fillText("mouseOver: " + Math.floor(mouseOverX) + "," + Math.floor(mouseOverY), 10, 40);

        // show boost enabled
        if (keyboardScrollBoost === true) {
            ctx.fillText("Scroll Boost", 10, canvas.height - 25);
        }
    };

    drawTiles();
    drawDeveloperOverlay();
}; // Draw everything

var init = function() {
    console.log('init called');
    initCanvas();
    initTiles();
    initMap();
    handleKeyboard();
    handleMouse();

    main();

    // Let's play this game!
    then = Date.now();
    setInterval(main, 1); // Execute as fast as possible
};

var main = function() {
    console.log('main called');
    //now = Date.now();
    //delta = now - then;



    var fps = 35;
    var now;
    //var then = Date.now();
    var interval = 1000 / fps;
    var delta;




    now = Date.now();
    delta = now - then;

    if (delta > interval) {
        // update time stuff        
        then = now - (delta % interval);

        update();
        requestAnimationFrame(render);
    }
};// Main game loop
